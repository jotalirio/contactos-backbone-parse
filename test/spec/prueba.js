describe("Pruebas sobre el modelo", function () {
   it("Los getters y setters tienen que funcionar", function() {
        var c = new Contacto();
        c.set("nombre","Pepe");
        expect(c.get("nombre")).toEqual("Pepe");
   });
});
